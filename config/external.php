<?php

return [
    'grew' => [
       'binary' => '/Users/guillaum/.opam/last/bin/grew'
    ],

    'melt' => [
        'binary'  => '/usr/local/bin/MElt',
        'path'  =>  '/usr/local/bin/',
    ],

    'dep2pict' => [
        'binary' => '/Users/guillaum/.opam/last/bin/dep2pict',
    ],

    'conll_tool' => [
        'binary' => '/Users/guillaum/.local/bin/conll_tool',
    ],

    'dot' => [
        'binary' => '/opt/local/bin/dot',
    ],

    'surf' => [
        'grs' => '/users/guillaum/gitlab/grew/POStoSSQ/grs/surf_synt_main.grs',
        'strat' => 'main',
    ],

    'mix' => [
        'grs' => '/users/guillaum/gitlab/grew/SSQtoDSQ/grs/main_dsq.grs',
        'strat' => 'main',
    ],

    'deep' => [
        'grs' => '/Users/guillaum/gitlab/deep-sequoia/tools/sequoia_proj.grs',
        'strat' => 'deep',
    ],

    'dmrs' => [
        'grs' => '/users/guillaum/gitlab/grew/DSQtoDMRS/grs/dmrs_main.grs',
        'strat' => 'demo',
    ],

    'amr' => [
        'grs' => '/users/guillaum/gitlab/grew/DSQtoAMR/grs/amr_main.grs',
        'strat' => 'demo',
    ],
];
