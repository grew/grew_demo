<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use View, Config;

class ParseController extends Controller
{
    public function run_command($dir, $slug, $cmd) {
        if (env('APP_DEBUG', false) == 1) {
            $cmd_file = fopen($dir . "/commands","a");
            fwrite($cmd_file, "=========== $slug ===========\n");
            fwrite($cmd_file, "$cmd\n\n");
            fclose($cmd_file);
            $full = "$cmd 2> $dir/$slug.error_log";
            exec($full);
        } else {
            exec($cmd);
        }
    }

    public function parse(Request $request)
    {
        // Create a new local dir to store the conll and svg files
        $local_dir = "storage/".uniqid();
        $dir= public_path()."/".$local_dir;
        mkdir($dir, 0777);

        // Write a file "sentence.txt" in the new directory
        $file = fopen($dir . "/sentence.txt","w");
        fwrite($file, $request->input('sentence'));
        fwrite($file, "\n");
        fclose($file);

        $grew= Config::get('external.grew.binary');
        $d2p= Config::get('external.dep2pict.binary');
        $conll_tool=  Config::get('external.conll_tool.binary');
        $dot=  Config::get('external.dot.binary');
        $melt= Config::get('external.melt.binary');
        $melt_path= Config::get('external.melt.path');
        $brown_to_json = Config::get('external.brown_to_json');

        // run MElt
        $cmd = "export PATH=\$PATH:$melt_path; cat $dir/sentence.txt | $melt -L -T > $dir/sentence.melt";
        $this->run_command ($dir, "melt", $cmd);

        // run brown_to_json
        $cmd = "$brown_to_json $dir/sentence.melt $dir/sentence.json";
        $this->run_command ($dir, "brown_to_json", $cmd);

        // construct surface structure
        $grs = Config::get('external.surf.grs');
        $strat = Config::get('external.surf.strat');
        $cmd = "$grew transform -grs $grs -strat $strat -i $dir/sentence.json -o $dir/sentence.surf.conll";
        $this->run_command ($dir, "surf", $cmd);

        $cmd = "$d2p $dir/sentence.surf.conll $dir/sentence.surf.svg";
        $this->run_command ($dir, "d2p_surf", $cmd);

        if ($request->input('kind')!="Parse") {
          // construct mixed structure
          $grs = Config::get('external.mix.grs');
          $strat = Config::get('external.mix.strat');
          $cmd = "$grew transform -grs $grs -strat $strat -i $dir/sentence.surf.conll -o $dir/sentence.mix.conll";
          $this->run_command ($dir, "mix", $cmd);

          $cmd = "$d2p $dir/sentence.mix.conll $dir/sentence.mix.svg";
          $this->run_command ($dir, "d2p_mix", $cmd);

          // construct deep structure
          $grs = Config::get('external.deep.grs');
          $strat = Config::get('external.deep.strat');
          $cmd = "$grew transform -grs $grs -strat $strat -i $dir/sentence.mix.conll -o $dir/sentence.deep.conll";
          $this->run_command ($dir, "deep", $cmd);

          $cmd = "$d2p $dir/sentence.deep.conll $dir/sentence.deep.svg";
          $this->run_command ($dir, "d2p_deep", $cmd);

          if ($request->input('kind')=="DMRS") {
            // construct dmrs structure
            $grs = Config::get('external.dmrs.grs');
            $strat = Config::get('external.dmrs.strat');
            $cmd = "$grew transform -grs $grs -strat $strat -i $dir/sentence.deep.conll -o $dir/sentence.dmrs.conll";
            $this->run_command ($dir, "dmrs", $cmd);
            $cmd = "$d2p $dir/sentence.dmrs.conll $dir/sentence.dmrs.svg";
            $this->run_command ($dir, "d2p_dmrs", $cmd);
          } else {
              // construct amr structure
            $grs = Config::get('external.amr.grs');
            $strat = Config::get('external.amr.strat');
            $cmd = "$grew transform -grs $grs -strat $strat -i $dir/sentence.deep.conll -o $dir/sentence.amr.conll";
            $this->run_command ($dir, "amr", $cmd);
            $cmd = "$d2p $dir/sentence.amr.conll $dir/sentence.amr.svg";
            $this->run_command ($dir, "d2p_amr", $cmd);
          }
        }

        return view('parse', [
          'subdir' => $local_dir,
          'sentence' => $request->input('sentence'),
          'kind' => $request->input('kind')
        ]);

    }




}
