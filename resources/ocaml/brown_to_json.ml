(* -------------------------------------------------------------------------------- *)
(** input : "Le/DET/le petit/ADJ/petit chat/NC/chat dort/V/dormir ./PONCT/." *)

  let re = Str.regexp "/\\(ADJ\\|ADJWH\\|ADV\\|ADVWH\\|CC\\|CLO\\|CLR\\|CLS\\|CS\\|DET\\|DETWH\\|ET\\|I\\|NC\\|NPP\\|P\\|P\\+D\\|P\\+PRO\\|PONCT\\|PREF\\|PRO\\|PROREL\\|PROWH\\|V\\|VIMP\\|VINF\\|VPP\\|VPR\\|VS\\)/"

  let of_brown brown =
    let units = Str.split (Str.regexp " ") brown in
    let json_nodes =
      ("0", `Assoc [("form", `String "__0__")]) ::
      List.mapi (
        fun i item -> match Str.full_split re item with
          | [Str.Text form; Str.Delim xpos_string; Str.Text lemma] ->
            let xpos = String.sub xpos_string 1 ((String.length xpos_string)-2) in
            (string_of_int (i+1), `Assoc [("form", `String form); ("xpos", `String xpos); ("lemma", `String lemma)])
          | _ -> failwith (Printf.sprintf "[Graph.of_brown] Cannot parse Brown item >>>%s<<< (expected \"phon/POS/lemma\") in >>>%s<<<" item brown)
      ) units in
    let order = List.map (fun (id,_) -> `String id) json_nodes in
    `Assoc [("nodes", `Assoc json_nodes); ("order", `List order)]

let _ =
  match Sys.argv with
  | [|_; i ; o|] ->
    begin
    let in_ch = open_in i in
    let out_ch = open_out o in
    try
      while true do
        let brown = input_line in_ch in
        let json = of_brown brown in
        Printf.fprintf out_ch "%s\n%!" (Yojson.Basic.pretty_to_string json) 
      done
    with End_of_file -> close_in in_ch; close_out out_ch
  end
  | _ -> failwith "Wrong arguments"