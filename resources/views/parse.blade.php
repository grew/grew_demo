@extends('layout')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12" style="padding-bottom: 20px;">

          <p>Depuis cette interface, il est possible de tester l'analyse syntaxique réalisée par réécriture de graphes avec l'outil <a href="http://grew.fr"> Grew </a>.</p>
          <p>Le bouton <b>Parse</b> retourne une structure en dépendances syntaxiques au format <a href="http://deep-sequoia.inria.fr">Sequoia</a>.</p>
          <p>Les boutons <b><a href="https://en.wikipedia.org/wiki/Minimal_recursion_semantics">DMRS</a></b> et <b><a href="http://amr.isi.edu/">AMR</a></b> permettent de construire la représentation sémantique (en DMRS ou en AMR) en plusieurs étapes gérées pas différents systèmes de réécriture de graphes.</p>

            <form method="post">
                {{ csrf_field() }} <!-- token handling -->
                <input name="sentence" value="{{ $sentence }}" placeholder="Entrez votre phrase ici !" id="sentence_textinput" style="width: 75%; height: 35px;" />
                <button class="btn-success btn" name= "kind" style="width: 8%" value= "Parse" type="submit">Parse</button>
                <button class="btn-success btn" name= "kind" style="width: 8%" value= "DMRS" type="submit">DMRS</button>
                <button class="btn-success btn" name= "kind" style="width: 8%" value= "AMR" type="submit">AMR</button>
            </form>
        </div>
    </div>

    @if($subdir!="")

    <!-- ==================================================== SURFACE ==================================================== -->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="pull-left"><a data-toggle="collapse" href="#surf_panel">Surface structure</a></div>
                    <div class="pull-right"><a href="#surfModal" class="btn btn-primary btn-xs" data-toggle="modal">Show CONLL</a></div>
                    <div class="clearfix"></div>
                </div>
                <div id="surf_panel" class="panel-collapse collapse in">
                    <div id ="surf" class="dep_struct">
                        <object type="image/svg+xml" data="{{ $subdir }}/sentence.surf.svg">
                        </object>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="surfModal" class="modal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Surface CONLL</h4>
                </div>
                <div class="modal-body">
                  <textarea rows="30" cols="122" readonly>{{ file_get_contents("$subdir/sentence.surf.conll") }}</textarea>
                </div>
            </div>
        </div>
    </div>

    @if($kind != "Parse")

    <!-- ==================================================== MIXED ==================================================== -->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="pull-left"><a data-toggle="collapse" href="#mix_panel">Mixed structure</a></div>
                    <div class="pull-right"><a href="#mixModal" class="btn btn-primary btn-xs" data-toggle="modal">Show CONLL</a></div>
                    <div class="clearfix"></div>
                </div>
                <div id="mix_panel" class="panel-collapse collapse">
                    <div id ="mix" class="dep_struct">
                        <object type="image/svg+xml" data="{{ $subdir }}/sentence.mix.svg">
                        </object>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="mixModal" class="modal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Mixed CONLL</h4>
                </div>
                <div class="modal-body">
                  <textarea rows="30" cols="122" readonly>{{ file_get_contents("$subdir/sentence.mix.conll") }}</textarea>
                </div>
            </div>
        </div>
    </div>

    <!-- ==================================================== DEEP ==================================================== -->
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="pull-left"><a data-toggle="collapse" href="#deep_panel">Deep structure</a></div>
                    <div class="pull-right"><a href="#deepModal" class="btn btn-primary btn-xs" data-toggle="modal">Show CONLL</a></div>
                    <div class="clearfix"></div>
                </div>
                <div id="deep_panel" class="panel-collapse collapse">
                    <div id ="deep" class="dep_struct">
                        <object type="image/svg+xml" data="{{ $subdir }}/sentence.deep.svg">
                        </object>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="deepModal" class="modal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Deep CONLL</h4>
                </div>
                <div class="modal-body">
                  <textarea rows="30" cols="122" readonly>{{ file_get_contents("$subdir/sentence.deep.conll") }}</textarea>
                </div>
            </div>
        </div>
    </div>

    <!-- ==================================================== AMR ==================================================== -->
    @if($kind == "AMR")
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="pull-left"><a data-toggle="collapse" href="#amr_panel">AMR structure</a></div>
                    <div class="pull-right"><a href="#amrModal" class="btn btn-primary btn-xs" data-toggle="modal">Show CONLL</a></div>
                    <div class="clearfix"></div>
                </div>
                <div id="amr_panel" class="panel-collapse collapse in">
                    <div id ="amr" class="dep_struct">
                        <object type="image/svg+xml" data="{{ $subdir }}/sentence.amr.svg">
                        </object>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="amrModal" class="modal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">AMR CONLL</h4>
                </div>
                <div class="modal-body">
                  <textarea rows="30" cols="122" readonly>{{ file_get_contents("$subdir/sentence.amr.conll") }}</textarea>
                </div>
            </div>
        </div>
    </div>
    @endif

    <!-- ==================================================== DMRS ==================================================== -->
    @if($kind == "DMRS")
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="pull-left"><a data-toggle="collapse" href="#dmrs_panel">DMRS structure</a></div>
                    <div class="pull-right"><a href="#dmrsModal" class="btn btn-primary btn-xs" data-toggle="modal">Show CONLL</a></div>
                    <div class="clearfix"></div>
                </div>
                <div id="dmrs_panel" class="panel-collapse collapse in">
                    <div id ="dmrs" class="dep_struct">
                        <object type="image/svg+xml" data="{{ $subdir }}/sentence.dmrs.svg">
                        </object>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="dmrsModal" class="modal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">DMRS CONLL</h4>
                </div>
                <div class="modal-body">
                  <textarea rows="30" cols="122" readonly>{{ file_get_contents("$subdir/sentence.dmrs.conll") }}</textarea>
                </div>
            </div>
        </div>
    </div>
    @endif

    @endif

    @endif

</div>
@endsection
